//
//  ViewController.m
//  DropDownMenu
//
//  Created by Click Labs134 on 10/5/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSMutableArray *nameList;
UIButton *btn;
UITableViewCell *cell;



@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *background;
@property (strong, nonatomic) IBOutlet UIButton *onClickButton;
@property (strong, nonatomic) IBOutlet UITableView *listOfNames;

@end

@implementation ViewController
@synthesize background;
@synthesize onClickButton;
@synthesize listOfNames;

- (void)viewDidLoad {
    [super viewDidLoad];
    nameList=[NSMutableArray arrayWithObjects:@"Suchismita",@"Vijay",@"Pooja",@"Prince",@"Param",@"Akshay",nil];
    
    listOfNames.hidden=YES;
    [onClickButton addTarget:self action:@selector(showTableView:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view, typically from a nib.
    
}

-(void)showTableView:(UIButton *)sender
{
    listOfNames.hidden=NO;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return nameList.count;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        [nameList removeObjectAtIndex:indexPath.row];
        [listOfNames deleteRowsAtIndexPaths:[NSMutableArray arrayWithObjects:indexPath, nil ] withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(void)deleteRow:(UIButton *)sender

{
    [listOfNames setEditing:YES animated:YES];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell=[tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.backgroundColor=[UIColor blueColor];
    cell.textLabel.text=nameList[indexPath.row];
    cell.textColor=[UIColor whiteColor];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [onClickButton setTitle:nameList[indexPath.row] forState:UIControlStateNormal];
    listOfNames.hidden=YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
